# -*- coding: utf-8 -*-
"""
tsukkomishere
Copyright (C) 2016  Yuval Langer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import scrapy
from urllib.parse import urlparse
import json
from tsukkomishere.items import TsukkomiPage
import logging

MANGA_HERE_DOMAIN = 'www.mangahere.co'


class MangahereSpider(scrapy.Spider):
    name = "mangahere"
    allowed_domains = [MANGA_HERE_DOMAIN]

    def __init__(self, url=None, *args, **kwargs):
        logging.debug('__init__')
        super().__init__(*args, **kwargs)
        if not url:
            return
        self.start_urls = [url]

    def start_requests(self):
        logging.debug('start_requests')
        for url in self.start_urls:
            meta = self.url_to_page(url)
            if meta.get('manga_page_id') not in ['1', None]:
                yield scrapy.Request(
                    'http://{domain_name}'
                    '/manga'
                    '/{manga_id}'
                    '/{manga_chapter_id}'
                    '/{manga_page_id}.html'.format(
                        domain_name=MANGA_HERE_DOMAIN,
                        **meta),
                    callback=self.parse_page)
            elif meta.get('manga_page_id') == '1':
                yield scrapy.Request(
                    'http://{domain_name}'
                    '/manga'
                    '/{manga_id}'
                    '/{manga_chapter_id}/'.format(
                        domain_name=MANGA_HERE_DOMAIN,
                        **meta),
                    callback=self.parse_chapter)
            else:
                yield scrapy.Request(
                    'http://{domain_name}'
                    '/manga'
                    '/{manga_id}/'.format(
                        domain_name=MANGA_HERE_DOMAIN,
                        **meta),
                    callback=self.parse_manga)


    @staticmethod
    def url_to_page(url):
        logging.debug('url_to_page')
        keys = ['manga_id', 'manga_chapter_id', 'manga_page_id']
        logging.debug(url)
        parsed_path = urlparse(url).path.split('/')[2:]
        logging.debug(parsed_path)
        meta = dict(zip(keys, parsed_path))
        if meta.get('manga_page_id') is not None:
            if meta['manga_page_id'] == '':
                meta['manga_page_id'] = '1'
            elif meta['manga_page_id'].endswith('.html'):
                meta['manga_page_id'] = meta['manga_page_id'][:-5]
        meta = {k: v for k, v in meta.items() if v != ''}
        logging.debug(meta)
        return meta

    def parse_manga(self, response):
        logging.debug('parse_manga')
        chapter_url_iter = response.xpath(
            '//div[@class="detail_list"]'
            '//a[starts-with(@href,'
            '"http://{domain_name}/manga/{manga_id}/")]/@href'.format(
                domain_name=MANGA_HERE_DOMAIN,
                **self.url_to_page(response.url))).extract()

        for chapter_url in chapter_url_iter:
            yield scrapy.Request(
                chapter_url,
                callback=self.parse_chapter)

    def parse_chapter(self, response):
        logging.debug('parse_chapter')
        page_url_iter = response.xpath(
            '//select[@class="wid60"]'
        )[0].xpath('option/@value').extract()

        yield from self.parse_page(response)

        for page_url in page_url_iter:
            yield scrapy.Request(
                page_url,
                callback=self.parse_page)

    def parse_page(self, response):
        logging.debug('parse_page')
        new_meta = self.url_to_page(response.url)
        new_meta.update(
            scan_id=self.extract_scan_id(response),
            tsukkomi_page_id=1)
        logging.debug('

        yield scrapy.Request(
            'http://{domain_name}'
            '/ajax/tsukkomi_read.php?'
            'scan_id={scan_id}&'
            'page={tsukkomi_page_id}'.format(
                domain_name=MANGA_HERE_DOMAIN,
                **new_meta),
            meta=new_meta,
            callback=self.parse_tsukkomi)

    def extract_scan_id(self, response: scrapy.http.Response) -> str:
        logging.debug('extract_scan_id')
        script_source_iter = response.xpath('//script/text()').extract()
        return next(
            script_source
            for script_source in script_source_iter
            if 'scan_id = "' in script_source
        ).split('scan_id = "', 1)[1].split('"', 1)[0]

    def parse_tsukkomi(self, response):
        logging.debug('parse_tsukkomi')
        tsukkomi_data = json.loads(response.body.decode())

        if tsukkomi_data == []:
            return []

        yield TsukkomiPage(
            tsukkomi_data=tsukkomi_data,
            manga_page_id=response.meta['manga_page_id'],
            scan_id=response.meta['scan_id'],
            tsukkomi_page_id=response.meta['tsukkomi_page_id'],
            manga_chapter_id=response.meta['manga_chapter_id'],
            manga_id=response.meta['manga_id'])

        if response.meta['tsukkomi_page_id'] < int(tsukkomi_data['max_page']):
            new_meta = response.meta.copy()
            new_meta['tsukkomi_page_id'] += 1
            new_url = (
                'http://{domain_name}'
                '/ajax/tsukkomi_read.php?'
                'scan_id={scan_id}&'
                'page={tsukkomi_page_id}').format(
                    domain_name=MANGA_HERE_DOMAIN,
                    **new_meta)
            yield scrapy.Request(
                new_url,
                meta=new_meta,
                callback=self.parse_tsukkomi)
